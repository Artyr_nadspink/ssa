import React, { Component } from 'react';
import {style} from './style.css'
import data from '../../data/data.json'
// import Card from '../card'
class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lanes: data.lanes,
            search:null,
            rezult:{},
        }
    }
    hanndleChangeSearck=(e)=>{
        this.state.search=e;
        this.setState({e:this.state.search});

    };
    searchFunc=()=>{

        let arr=[];
        let arr2=[];
        for(let key in this.state.lanes ){
            arr.push(this.state.lanes[key].cards);
        }
        for (let card in arr){
            for(let i=0;i<arr.length;i++){
                arr2.push(arr[card][i]);

            }
        }
        let filt_rezuul = arr2.filter(card => card!==undefined);

        let rezult = filt_rezuul.filter(card => this.state.search===card.title);
        this.state.rezult= rezult[0];
        this.setState({rezult:this.state.rezult});


    };

    render() {

        return (
            <div className='root_s '>
                <div className="search" onClick={(e)=>this.searchFunc()}></div>
                <input className='input_s' onChange={(e) => this.hanndleChangeSearck(e.target.value)} type="text"/>
                <p>
                  id:  {this.state.rezult!== undefined ? this.state.rezult.id :''}
                    <br/>
                    title: {this.state.rezult!== undefined ? this.state.rezult.title :''}
                    <br/>
                    description: {this.state.rezult!== undefined ? this.state.rezult.description :''}
                </p>
            </div>
        )
    }
}
export default Search;