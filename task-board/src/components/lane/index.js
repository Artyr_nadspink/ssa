import React , { Component }  from 'react';

import data from '../../data/data'

class Lane extends Component {
    constructor(props) {
        super(props)
        this.state = {
            lanes:data.lanes,
        }
    }
    render() {



        return (
            <div className="App">
                <div> количество "lanes"
                {
                this.state.lanes.map((lane)=>{

                    return <ul key={lane.id} >
                        <li >{lane.title}</li>
                    </ul>
                })
                }
                </div>
            </div>
        );
    }
}

export default Lane;
