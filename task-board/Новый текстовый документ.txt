import React, {Component} from 'react';
import data from '../../data/data'
import style from './style.css'
import Card from '../card'


class Container extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lanes: data.lanes,
            value_inp: null,
            // gg: null,
        }

    }

    handleChange = (e, id, id_lane, name) => {
        let index_lane = null;
        let arr = this.state.lanes;
        arr.forEach(function (item, i, arr) {
            if (item.id === id_lane) {
                index_lane = i;
            }
        });
        arr[index_lane].cards.forEach(function (item, i, arr) {
            if (item.id === id) {
                item[name] = e
            }
        });

        return arr
    };

    editCard = (handleChange) => {
        this.setState({handleChange});

    };

    addCard = (id) => {

        let pusObg = {};
        let index_lane = null;
        let arr = this.state.lanes;
        arr.forEach(function (item, i, arr) {
            if (item.id === id) {
                index_lane = i;
            }
            pusObg = {
                "id": "Milk" + Math.floor(Math.random(1, 10000000) * 1000),
                "title": "Buy milk",
                "label": "15 mins",
                "description": "2 Gallons of milk at the Deli store",
                "priority": 3
            };
        });

        arr[index_lane].cards.push(pusObg);

        arr = arr[index_lane];
        this.setState({arr});

    };

    deleteFunc = (id_card, id_lane) => {
        let arr = this.state.lanes;
        let index_lane = null;
        let index_card = null;
        arr.forEach(function (item, i, arr) {
            if (item.id === id_lane) {
                index_lane = i;
            }
        });
        let arr1 = this.state.lanes[index_lane].cards;

        const newArr = arr1.filter((item) => {
            return item.id !== id_card;
        });

        this.state.lanes[index_lane].cards = newArr;
        this.setState({newArr: this.state.lanes[index_lane].cards});

    };


    render() {
console.log(this.state.lanes.cards);

        return (
            <div>

                <div className='root'>

                    {
                        this.state.lanes.map((lane) => {
                            return <div className="container" key={lane.id}>
                                <div className="top">
                                    <h1>{lane.title}</h1>
                                    <div className="wrapper">
                                        <div className="add" onClick={(e) => this.addCard(lane.id)}>+</div>
                                        <div className="quantity">{lane.cards.length}</div>
                                    </div>
                                </div>
                                <div className="content">
                                    {lane.cards.map((card) => {
                                        return <Card key={card.id}
                                                     props={card} delet={this.deleteFunc}
                                                     number={lane} edit={this.editCard}
                                                     handleChange={this.handleChange}
                                                     value={this.state.value_inp}

                                        />
                                    })}
                                </div>
                            </div>

                        })
                    }

                </div>
            </div>
        );
    }
}

export default Container;










import React, {Component} from 'react';
import style from './style.css'

const Card = ({props, delet, number,edit,handleChange}) => {
    let clas_prior = [0, 'high', 'normal', 'low', 'readi'];


    return (

        <div className={'root_c ' + clas_prior[props.priority]}>
            <div className='title_c'>{props.title}</div>
            <div className='description_c'>{props.description}</div>
            <div className="function_block">
                <form>
                    title <br/>
                <input type="text"
                       value={undefined}
                       name='title'
                       onChange={(e)=>handleChange(e.target.value,props.id,number.id,'title')}/>
                    description
                <input type="text"
                       value={undefined}
                       name='discription'
                       onChange={(e)=>handleChange(e.target.value,props.id,number.id,'description')}/>
                </form>

                <div className="del" onClick={(e) => delet(props.id,number.id)}></div>

                <div className="edit" onClick={(e) => edit(props.id,number.id)}></div>

            </div>
        </div>
    )
}
export default Card;









